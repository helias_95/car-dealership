import { BrowserRouter, Routes, Route } from "react-router-dom";
import MainPage from "./MainPage";
import Nav from "./Nav";
import ListManufacturer from "./components/inventory/ListManufacturers";
import CreateManufacturer from "./components/inventory/CreateManufacturer";
import ListModels from "./components/inventory/ListModels";
import CreateModel from "./components/inventory/CreateModel";
import ListAutomobiles from "./components/inventory/ListAutomobiles";
import CreateAutomobile from "./components/inventory/CreateAutomobile";
import ListTechnicians from "./components/service/ListTechnicians";
import CreateTechnician from "./components/service/CreateTechnician";
import ListServiceAppointments from "./components/service/ListServiceAppointments";
import CreateServiceAppointment from "./components/service/CreateServiceAppointment";
import ListServiceHistory from "./components/service/ListServiceHistory";
import CustomerList from "./components/sales/CustomerList";
import CustomerForm from "./components/sales/CreateCustomer";
import SalespeopleList from "./components/sales/ListSalesPerson";
import CreateSalesPerson from "./components/sales/CreateSalesPerson";
import ListSales from "./components/sales/ListAllSales";
import SaleHistory from "./components/sales/SalesPersonHistory";
import RecordSale from "./components/sales/RecordNewSale";
function App() {
	return (
		<BrowserRouter>
			<Nav />
			<div className="container">
				<Routes>
					<Route path="/" element={<MainPage />} />
					<Route
						path="manufacturers"
						element={<ListManufacturer />}
					/>
					<Route
						path="manufacturer/create"
						element={<CreateManufacturer />}
					/>
					<Route path="models" element={<ListModels />} />
					<Route path="model/create" element={<CreateModel />} />
					<Route path="automobiles" element={<ListAutomobiles />} />
					<Route
						path="automobile/create/"
						element={<CreateAutomobile />}
					/>
					<Route path="technicians" element={<ListTechnicians />} />
					<Route
						path="technician/create"
						element={<CreateTechnician />}
					/>
					<Route
						path="appointments"
						element={<ListServiceAppointments />}
					/>
					<Route
						path="appointment/create"
						element={<CreateServiceAppointment />}
					/>
					<Route
						path="service-history"
						element={<ListServiceHistory />}
					/>
					<Route
						path="customers"
						element={<CustomerList/>}
					/>
					<Route 
						path="customer/create"
						element={<CustomerForm />}
					/>
					<Route
						path="salespeople"
						element= {<SalespeopleList/>}
					/>
					<Route 
						path="salesperson/create"
						element= {<CreateSalesPerson/>}
					/>
					<Route
						path="sales"
						element= {<ListSales/>}
					/>
					<Route
						path="sales-history"
						element={<SaleHistory/>}
					/>
					<Route
						path="sale/create"
						element={<RecordSale/>}
					/>

				</Routes>
			</div>
		</BrowserRouter>
	);
}

export default App;
