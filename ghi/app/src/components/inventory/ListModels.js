import { useState, useEffect } from "react";

export default function ListModels() {
	const [models, setModels] = useState([]);

	const getData = async () => {
		const url = "http://localhost:8100/api/models/";
		const response = await fetch(url);

		if (response.ok) {
			const data = await response.json();
			setModels(data.models);
		}
	};

	useEffect(() => {
		getData();
	}, []);

	return (
		<>
			<div>
				<h1>Models</h1>
			</div>
			<table className="table table-striped">
				<thead>
					<tr>
						<th>Name</th>
						<th>Manufacturer</th>
						<th>Picture</th>
					</tr>
				</thead>
				<tbody>
					{models?.map((model) => (
						<tr key={model.id} value={model.id}>
							<td>{model.name}</td>
							<td>{model.manufacturer.name}</td>
							<td>
								<img
									src={model.picture_url}
									alt=""
									width="500"
								/>
							</td>
						</tr>
					))}
				</tbody>
			</table>
		</>
	);
}
