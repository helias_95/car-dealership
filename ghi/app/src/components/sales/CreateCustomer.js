import React, {useState} from 'react'
import {useNavigate} from 'react-router-dom'
function CustomerForm(){
    const navigate = useNavigate();
    const [firstName, setFirstName] = useState('');
    const [lastName, setLastName] = useState('');
    const [address, setAddress] = useState('');
    const [phoneNum, setPhoneNum] = useState('');
    const handleFirstNameChange = (e) => {
        const value = e.target.value;
        setFirstName(value)
    }
    const handleLastNameChange = (e) => {
        const value = e.target.value;
        setLastName(value)
    }
    const handleAddressChange = (e) => {
        const value = e.target.value;
        setAddress(value)
    }
    const handlePhoneNumChange = (e) => {
        const value = e.target.value;
        setPhoneNum(value)
    }
    const handleSubmit = async (event) => {
        event.preventDefault();
        const data = {
            first_name: firstName,
            last_name: lastName,
            address: address,
            phone_number: phoneNum,
        }
        const customerUrl = "http://localhost:8090/api/customers/"
        const fetchConfig = {
            method: "post",
            body: JSON.stringify(data),
            headers: {
                "Content-Type": "application/json",
            }
        }
        const response = await fetch(customerUrl, fetchConfig)
        if(response.ok){
            setFirstName('');
            setLastName('');
            setAddress('');
            setPhoneNum('');
            navigate("/customers")

        }
    }




    return(
        <div className='container'>
        <div className="row">
        <div className="offset-3 col-6">
            <div className="shadow p-4 mt-4">
                <h1>Create A New Customer</h1>
                <form onSubmit={handleSubmit} id="create-customer-form">
                    <div className='form-floating mb-3'>
                        <input value={firstName} onChange={handleFirstNameChange} placeholder="First Name" required type="text" name="firstname" id="firstname" className="form-control"/>
                        <label htmlFor="firstname">First Name</label>
                    </div>
                    <div className='form-floating mb-3'>
                        <input value={lastName} onChange={handleLastNameChange} placeholder="Last Name" required type="text" name="lastname" id="lastname" className="form-control"/>
                        <label htmlFor="lastname">Last Name</label>
                    </div>
                    <div className='form-floating mb-3'>
                        <input value={address} onChange={handleAddressChange} placeholder="Address" required type="text" name="address" id="address" className="form-control"/>
                        <label htmlFor="address">Address</label>
                    </div>
                    <div className='form-floating mb-3'>
                        <input value={phoneNum} onChange={handlePhoneNumChange} placeholder="Phone Number" required type="text" name="phonenum" id="phonenum" className="form-control"/>
                        <label htmlFor="phonenum">Phone Number</label>
                    </div>
                    <div>
                        <button className='btn btn-success'>Create</button>
                    </div>
                    
                </form>
            </div>
            </div>
        </div>
        </div>
    )

}

export default CustomerForm