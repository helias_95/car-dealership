import {useState, useEffect} from 'react'

function SalespeopleList() {
    const [salespeople, setSalesPeople] = useState([]);

    const fetchSalesPeople = async () => {
        const url = "http://localhost:8090/api/salespeople/";
        const response = await fetch(url);
        if(response.ok){
            const data = await response.json();
            setSalesPeople(data.salespeople)
        }
    }
    useEffect(() => {
        fetchSalesPeople()
    }, [])

    return (
        <div className='h-screen'>
        <br></br>
        <h1>Salespeople</h1>
        <br></br>
        <div>
            <table className="table table-striped">
                <thead >
                    <tr>
                        <th>ID</th>
                        <th>First Name</th>
                        <th>Last Name</th>
                        <th>Employee ID</th>
                        <th></th>
                    </tr>
                </thead>
                <tbody>
                    {salespeople.map(salesperson => {
                        return(
                            <tr key={salesperson.id} value = {salesperson.id}>
                                <td>{ salesperson.id }</td>
                                <td>{ salesperson.first_name }</td>
                                <td>{ salesperson.last_name }</td>
                                <td>{ salesperson.employee_id }</td>
                                
                                
                            </tr>
                        );
                    })}
                </tbody>
            </table>
        </div>

    </div> 
    )
}

export default SalespeopleList