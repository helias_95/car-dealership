import { useState, useEffect } from "react";
import { useNavigate } from "react-router-dom";

export default function CreateTechnician() {
	const navigate = useNavigate();
	const [technicians, setTechnicians] = useState([]);
	const [employee_id, setEmployee_id] = useState("");
	const [first_name, setFirst_name] = useState("");
	const [last_name, setLast_name] = useState("");

	// event handlers
	const handleEmployee_idChange = (event) => {
		setEmployee_id(event.target.value);
	};

	const handleFirst_nameChange = (event) => {
		setFirst_name(event.target.value);
	};

	const handleLast_nameChange = (event) => {
		setLast_name(event.target.value);
	};

	const handleSubmit = async (event) => {
		event.preventDefault();

		const data = {
			employee_id: employee_id,
			first_name: first_name,
			last_name: last_name,
		};

		// on submit, send a POST request to this server
		const url = "http://localhost:8080/api/technicians/";
		const fetchConfig = {
			method: "POST",
			headers: {
				"Content-Type": "application/json",
			},
			body: JSON.stringify(data),
		};

		const response = await fetch(url, fetchConfig);
		// if the response is ok, clear inputs and navigate to the technicians page
		if (response.ok) {
			const data = await response.json();
			setTechnicians([...technicians, data]);
			setEmployee_id("");
			setFirst_name("");
			setLast_name("");

			navigate("/technicians");
		}
	};

	// get initial data from this server
	const getData = async () => {
		const url = "http://localhost:8080/api/technicians/";
		const response = await fetch(url);
		const data = await response.json();
		setTechnicians(data.technicians);
	};

	// call getData() when the component mounts
	useEffect(() => {
		getData();
	}, []);

	return (
		<>
			<div className="container">
				<div className="row">
					<div className="offset-3 col-6">
						<div className="shadow p-4 mt-4">
							<h1>Add a Technician</h1>
							<form onSubmit={handleSubmit}>
								<div className="form-floating mb-3">
									<input
										value={first_name}
										onChange={handleFirst_nameChange}
										placeholder="First Name"
										required
										type="text"
										name="first_name"
										id="first_name"
										className="form-control"
									/>
									<label htmlFor="first_name">
										First Name
									</label>
								</div>
								<div className="form-floating mb-3">
									<input
										value={last_name}
										onChange={handleLast_nameChange}
										placeholder="Last Name"
										required
										type="text"
										name="last_name"
										id="last_name"
										className="form-control"
									/>
									<label htmlFor="last_name">Last Name</label>
								</div>
								<div className="form-floating mb-3">
									<input
										value={employee_id}
										onChange={handleEmployee_idChange}
										placeholder="Employee ID"
										required
										type="text"
										name="employee_id"
										id="employee_id"
										className="form-control"
									/>
									<label htmlFor="employee_id">
										Employee ID
									</label>
								</div>

								<button
									type="submit"
									className="btn btn-success"
								>
									Add
								</button>
							</form>
						</div>
					</div>
				</div>
			</div>
		</>
	);
}
