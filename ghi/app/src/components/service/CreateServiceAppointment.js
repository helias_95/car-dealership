import { useState, useEffect } from "react";
import { useNavigate } from "react-router-dom";

export default function CreateServiceAppointment() {
	const navigate = useNavigate();
	const [serviceAppointments, setServiceAppointments] = useState([]);
	const [technicians, setTechnicians] = useState([]);
	const [date, setDate] = useState("");
	const [time, setTime] = useState("");
	const [reason, setReason] = useState("");
	const [vin, setVin] = useState("");
	const [customer, setCustomer] = useState("");
	const [technician, setTechnician] = useState("");

	// event handlers
	const handleDateChange = (event) => {
		setDate(event.target.value);
	};

	const handleTimeChange = (event) => {
		setTime(event.target.value);
	};

	const handleReasonChange = (event) => {
		setReason(event.target.value);
	};

	const handleVinChange = (event) => {
		setVin(event.target.value);
	};

	const handleCustomerChange = (event) => {
		setCustomer(event.target.value);
	};

	const handleTechnicianChange = (event) => {
		setTechnician(event.target.value);
	};

	const handleSubmit = async (event) => {
		event.preventDefault();

		const dateTime = new Date(`${date}T${time}`);
		const isoDateTime = dateTime.toISOString();

		const data = {
			date_time: isoDateTime,
			reason: reason,
			vin: vin,
			customer: customer,
			technician: technician,
			status: "Created",
		};

		const url = "http://localhost:8080/api/appointments/";
		const fetchConfig = {
			method: "POST",
			headers: {
				"Content-Type": "application/json",
			},
			body: JSON.stringify(data),
		};

		const response = await fetch(url, fetchConfig);
		if (response.ok) {
			const data = await response.json();
			setServiceAppointments([...serviceAppointments, data]);
			setDate("");
			setTime("");
			setReason("");
			setVin("");
			setCustomer("");
			setTechnician("");

			navigate("/appointments");
		}
	};

	// initial data pull on component mount
	const getData = async () => {
		const url = "http://localhost:8080/api/technicians/";
		const response = await fetch(url);
		const data = await response.json();
		setTechnicians(data.technicians);
	};

	// useEffect to run getData on component mount
	useEffect(() => {
		getData();
	}, []);

	return (
		<>
			<div className="container">
				<div className="row">
					<div className="offset-3 col-6">
						<div className="shadow p-4 mt-4">
							<h1>Create a Service Appointment</h1>
							<form
								onSubmit={handleSubmit}
								id="create-service-appointment-form"
							>
								<div className="form-floating mb-3">
									<input
										value={vin}
										onChange={handleVinChange}
										placeholder="VIN"
										required
										type="text"
										name="vin"
										id="vin"
										className="form-control"
									/>
									<label htmlFor="vin">Automobile VIN</label>
								</div>
								<div className="form-floating mb-3">
									<input
										value={customer}
										onChange={handleCustomerChange}
										placeholder="Customer"
										required
										type="text"
										name="customer"
										id="customer"
										className="form-control"
									/>
									<label htmlFor="customer">Customer</label>
								</div>
								<div className="form-floating mb-3">
									<input
										value={date}
										onChange={handleDateChange}
										required
										type="date"
										name="date"
										id="date"
										className="form-control"
									/>
									<label htmlFor="date">Date</label>
								</div>
								<div className="form-floating mb-3">
									<input
										value={time}
										onChange={handleTimeChange}
										required
										type="time"
										name="time"
										id="time"
										className="form-control"
									/>
									<label htmlFor="time">Time</label>
								</div>
								<div className="form-floating mb-3">
									<select
										value={technician}
										onChange={handleTechnicianChange}
										required
										name="technician"
										id="technician"
										className="form-select"
									>
										<option value="">
											Choose a technician...
										</option>
										{/* map over the technicians array */}
										{technicians.map((technician) => {
											// create a full name variable
											const fullName = `${technician.first_name} ${technician.last_name}`;
											return (
												<option
													key={technician.id}
													value={technician.id}
												>
													{fullName}
												</option>
											);
										})}
									</select>
								</div>
								<div className="form-floating mb-3">
									<input
										value={reason}
										onChange={handleReasonChange}
										placeholder="Reason"
										required
										type="text"
										name="reason"
										id="reason"
										className="form-control"
									/>
									<label htmlFor="reason">Reason</label>
								</div>
								<button
									type="submit"
									className="btn btn-success"
								>
									Create
								</button>
							</form>
						</div>
					</div>
				</div>
			</div>
		</>
	);
}
