import { useState, useEffect } from "react";

export default function ListServiceAppointments() {
	const [serviceAppointments, setServiceAppointments] = useState([]);
	const [automobiles, setAutomobiles] = useState([]);

	// Fetch the service appointments and automobiles
	const getData = async () => {
		const url = "http://localhost:8080/api/appointments/";
		const response = await fetch(url);

		if (response.ok) {
			const data = await response.json();
			setServiceAppointments(data.appointments);
		}

		const url2 = "http://localhost:8100/api/automobiles/";
		const response2 = await fetch(url2);

		if (response2.ok) {
			const data2 = await response2.json();
			setAutomobiles(data2.autos);
		}
	};

	// Fetch the data on component mount
	useEffect(() => {
		getData();
	}, []);

	// PUT request to finish an appointment
	const handleFinishAppointment = async (id) => {
		const url = `http://localhost:8080/api/appointments/${id}/finish/`;
		const response = await fetch(url, {
			method: "PUT",
			headers: {
				"Content-Type": "application/json",
			},
		});

		if (response.ok) {
			// After finishing, fetch the updated list and filter it
			getData();
		}
	};

	// PUT request to cancel an appointment
	const handleCancelAppointment = async (id) => {
		const url = `http://localhost:8080/api/appointments/${id}/cancel/`;
		const response = await fetch(url, {
			method: "PUT",
			headers: {
				"Content-Type": "application/json",
			},
		});

		if (response.ok) {
			// After cancelling, fetch the updated list and filter it
			getData();
		}
	};

	// Format date and time from ISO string
	const formatDateTime = (isoString) => {
		const dateTime = new Date(isoString);
		const date = dateTime.toLocaleDateString();
		const time = dateTime.toLocaleTimeString();
		return { date, time };
	};

	// Filter the service appointments based on their status
	const filteredAppointments = serviceAppointments.filter(
		(appointment) =>
			appointment.status !== "Finished" &&
			appointment.status !== "Cancelled"
	);

	return (
		<>
			<h1>Service Appointments</h1>
			<table className="table table-striped">
				<thead>
					<tr>
						<th>VIN</th>
						<th>is VIP?</th>
						<th>Customer</th>
						<th>Date</th>
						<th>Time</th>
						<th>Technician</th>
						<th>Reason</th>
						<th>Status</th>
					</tr>
				</thead>
				<tbody>
					{/* map over the filtered appointments */}
					{filteredAppointments.map((appointment) => {
						// create variable for technician's full name
						const technicianFullName = `${appointment.technician.first_name} ${appointment.technician.last_name}`;

						// Format the date and time
						const { date, time } = formatDateTime(
							appointment.date_time
						);

						// Find the corresponding automobile
						const automobile = automobiles.find(
							(auto) => auto.vin === appointment.vin
						);

						return (
							<tr key={appointment.id} value={appointment.id}>
								<td>{appointment.vin}</td>
								<td>
									{/* from the automobile list, if the automobile is sold, display "Yes", otherwise display "No" */}
									{automobile ? automobile.sold ? "Yes" : "No" : "No"}
								</td>
								<td>{appointment.customer}</td>
								<td>{date}</td>
								<td>{time}</td>
								<td>{technicianFullName}</td>
								<td>{appointment.reason}</td>
								<td>{appointment.status}</td>
								<td>
									<button
										className="btn btn-success"
										// when the button is clicked, call the handleFinishAppointment function with the appointment id
										onClick={() =>
											handleFinishAppointment(
												appointment.id
											)
										}
									>
										Finish
									</button>
									<button
										className="btn btn-danger"
										// when the button is clicked, call the handleCancelAppointment function with the appointment id
										onClick={() =>
											handleCancelAppointment(
												appointment.id
											)
										}
									>
										Cancel
									</button>
								</td>
							</tr>
						);
					})}
				</tbody>
			</table>
		</>
	);
}
