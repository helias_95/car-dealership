import React from "react";
import { NavLink } from "react-router-dom";

function Nav() {
	return (
		<nav className="navbar navbar-expand-xl navbar-dark bg-success">
			<div className="container-fluid">
				<NavLink className="navbar-brand" to="/">
					CarCar
				</NavLink>
				<button
					className="navbar-toggler"
					type="button"
					data-bs-toggle="collapse"
					data-bs-target="#navbarSupportedContent"
					aria-controls="navbarSupportedContent"
					aria-expanded="false"
					aria-label="Toggle navigation"
				>
					<span className="navbar-toggler-icon"></span>
				</button>

				<div
					className="collapse navbar-collapse justify-content-center"
					id="navbarSupportedContent"
				>
					<ul className="navbar-nav mb-2 mb-lg-0">
						<li className="nav-item dropdown">
							<button
								className="nav-link dropdown-toggle"
								id="navbarDropdown"
								data-bs-toggle="dropdown"
								aria-expanded="false"
								style={{
									backgroundColor: "transparent",
									border: "none",
									color: "rgba(255,255,255,.55)",
									transition: "color 0.2s",
								}}
								onMouseEnter={(e) => {
									e.target.style.color = "white";
								}}
								onMouseLeave={(e) => {
									e.target.style.color =
										"rgba(255, 255, 255, 0.55)";
								}}
							>
								Automobiles
							</button>
							<ul className="dropdown-menu">
								<li className="dropdown-item">
									<NavLink
										className="nav-link"
										to="automobiles"
										style={{ color: "black" }}
									>
										All Automobiles
									</NavLink>
								</li>
								<li className="dropdown-item">
									<NavLink
										className="nav-link"
										to="automobile/create"
										style={{ color: "black" }}
									>
										Create Automobile
									</NavLink>
								</li>
							</ul>
						</li>
						<li className="nav-item dropdown">
							<button
								className="nav-link dropdown-toggle"
								id="navbarDropdown"
								data-bs-toggle="dropdown"
								aria-expanded="false"
								style={{
									backgroundColor: "transparent",
									border: "none",
									color: "rgba(255,255,255,.55)",
									transition: "color 0.2s",
								}}
								onMouseEnter={(e) => {
									e.target.style.color = "white";
								}}
								onMouseLeave={(e) => {
									e.target.style.color =
										"rgba(255, 255, 255, 0.55)";
								}}
							>
								Manufacturers
							</button>
							<ul className="dropdown-menu">
								<li className="dropdown-item">
									<NavLink
										className="nav-link"
										to="manufacturers"
										style={{ color: "black" }}
									>
										All Manufacturers
									</NavLink>
								</li>
								<li className="dropdown-item">
									<NavLink
										className="nav-link"
										to="manufacturer/create"
										style={{ color: "black" }}
									>
										Create a Manufacturer
									</NavLink>
								</li>
							</ul>
						</li>
						<li className="nav-item dropdown">
							<button
								className="nav-link dropdown-toggle"
								id="navbarDropdown"
								data-bs-toggle="dropdown"
								aria-expanded="false"
								style={{
									backgroundColor: "transparent",
									border: "none",
									color: "rgba(255,255,255,.55)",
									transition: "color 0.2s",
								}}
								onMouseEnter={(e) => {
									e.target.style.color = "white";
								}}
								onMouseLeave={(e) => {
									e.target.style.color =
										"rgba(255, 255, 255, 0.55)";
								}}
							>
								Models
							</button>
							<ul className="dropdown-menu">
								<li className="dropdown-item">
									<NavLink
										className="nav-link"
										to="models"
										style={{ color: "black" }}
									>
										All Models
									</NavLink>
								</li>
								<li className="dropdown-item">
									<NavLink
										className="nav-link"
										to="model/create"
										style={{ color: "black" }}
									>
										Create a Model
									</NavLink>
								</li>
							</ul>
						</li>
						<li className="nav-item dropdown">
							<button
								className="nav-link dropdown-toggle"
								id="navbarDropdown"
								data-bs-toggle="dropdown"
								aria-expanded="false"
								style={{
									backgroundColor: "transparent",
									border: "none",
									color: "rgba(255,255,255,.55)",
									transition: "color 0.2s",
								}}
								onMouseEnter={(e) => {
									e.target.style.color = "white";
								}}
								onMouseLeave={(e) => {
									e.target.style.color =
										"rgba(255, 255, 255, 0.55)";
								}}
							>
								Technicians
							</button>
							<ul className="dropdown-menu">
								<li className="dropdown-item">
									<NavLink
										className="nav-link"
										to="technicians"
										style={{ color: "black" }}
									>
										All Technicians
									</NavLink>
								</li>
								<li className="dropdown-item">
									<NavLink
										className="nav-link"
										to="technician/create"
										style={{ color: "black" }}
									>
										Add Technician
									</NavLink>
								</li>
							</ul>
						</li>
						<li className="nav-item dropdown">
							<button
								className="nav-link dropdown-toggle"
								id="navbarDropdown"
								data-bs-toggle="dropdown"
								aria-expanded="false"
								style={{
									backgroundColor: "transparent",
									border: "none",
									color: "rgba(255,255,255,.55)",
									transition: "color 0.2s",
								}}
								onMouseEnter={(e) => {
									e.target.style.color = "white";
								}}
								onMouseLeave={(e) => {
									e.target.style.color =
										"rgba(255, 255, 255, 0.55)";
								}}
							>
								Service Appointments
							</button>
							<ul className="dropdown-menu">
								<li className="dropdown-item">
									<NavLink
										className="nav-link"
										to="appointments"
										style={{ color: "black" }}
									>
										All Service Appointments
									</NavLink>
								</li>
								<li className="dropdown-item">
									<NavLink
										className="nav-link"
										to="appointment/create"
										style={{ color: "black" }}
									>
										Create a Service Appointment
									</NavLink>
								</li>
								<li className="dropdown-item">
									<NavLink
										className="nav-link"
										to="service-history"
										style={{ color: "black" }}
									>
										Service History
									</NavLink>
								</li>
							</ul>
						</li>

						<li className="nav-item dropdown">
							<button
								className="nav-link dropdown-toggle"
								id="navbarDropdown"
								data-bs-toggle="dropdown"
								aria-expanded="false"
								style={{
									backgroundColor: "transparent",
									border: "none",
									color: "rgba(255,255,255,.55)",
									transition: "color 0.2s",
								}}
								onMouseEnter={(e) => {
									e.target.style.color = "white";
								}}
								onMouseLeave={(e) => {
									e.target.style.color =
										"rgba(255, 255, 255, 0.55)";
								}}
							>
								Customers
							</button>
							<ul className="dropdown-menu">
								<li className="dropdown-item">
									<NavLink
										className="nav-link"
										to="customers"
										style={{ color: "black" }}
									>
										All Customers
									</NavLink>
								</li>
								<li className="dropdown-item">
									<NavLink
										className="nav-link"
										to="customer/create"
										style={{ color: "black" }}
									>
										Create a Customer
									</NavLink>
								</li>
							</ul>
						</li>
						<li className="nav-item dropdown">
							<button
								className="nav-link dropdown-toggle"
								id="navbarDropdown"
								data-bs-toggle="dropdown"
								aria-expanded="false"
								style={{
									backgroundColor: "transparent",
									border: "none",
									color: "rgba(255,255,255,.55)",
									transition: "color 0.2s",
								}}
								onMouseEnter={(e) => {
									e.target.style.color = "white";
								}}
								onMouseLeave={(e) => {
									e.target.style.color =
										"rgba(255, 255, 255, 0.55)";
								}}
							>
								Salespeople
							</button>
							<ul className="dropdown-menu">
								<li className="dropdown-item">
									<NavLink
										className="nav-link"
										to="salespeople"
										style={{ color: "black" }}
									>
										All Salespeople
									</NavLink>
								</li>
								<li className="dropdown-item">
									<NavLink
										className="nav-link"
										to="salesperson/create"
										style={{ color: "black" }}
									>
										Create a Salesperson
									</NavLink>
								</li>
							</ul>
						</li>
						
						<li className="nav-item dropdown">
							<button
								className="nav-link dropdown-toggle"
								id="navbarDropdown"
								data-bs-toggle="dropdown"
								aria-expanded="false"
								style={{
									backgroundColor: "transparent",
									border: "none",
									color: "rgba(255,255,255,.55)",
									transition: "color 0.2s",
								}}
								onMouseEnter={(e) => {
									e.target.style.color = "white";
								}}
								onMouseLeave={(e) => {
									e.target.style.color =
										"rgba(255, 255, 255, 0.55)";
								}}
							>
								Sales
							</button>
							<ul className="dropdown-menu">
								<li className="dropdown-item">
									<NavLink
										className="nav-link"
										to="sales"
										style={{ color: "black" }}
									>
										All Sales
									</NavLink>
								</li>
								<li className="dropdown-item">
									<NavLink
										className="nav-link"
										to="sales-history"
										style={{ color: "black" }}
									>
										Sales History
									</NavLink>
								</li>
								<li className="dropdown-item">
									<NavLink
										className="nav-link"
										to="sale/create"
										style={{ color: "black" }}
									>
										Record a Sale
									</NavLink>
								</li>
							</ul>
						</li>
					</ul>
				</div>
			</div>
		</nav>
	);
}

export default Nav;
