# CarCar

---

## Team

#### Services:

Hector Elias

#### Sales:

Lily Grey

---

## Design

![Cory and Jacks Diagram](/assets/project-beta-diagram.png "Project Beta Diagram")

## Service microservice (service\_ rest)

**Technician Model**:

-   This model represents the technicians who perform various services on automobiles.
-   It includes fields for the technician's first name, last name, and a unique employee ID.

**AutomobileVO Model**:

-   This model represents automobiles within the service microservice.
-   It includes a Vehicle Identification Number (VIN) and a field to track whether the automobile is sold.

**Appointment**:

-   This model represents service appointments for automobiles.
-   It includes fields for the date and time of the appointment, the reason for the appointment, appointment status, VIN of the associated automobile, and customer details.
-   The technician field establishes a foreign key relationship with the Technician model, linking each appointment to a specific technician.

**Integration**:
The Appointment model in the service microservice references the Technician model using a foreign key relationship, allowing appointments to be associated with specific technicians.

## **Inventory Microservices (inventory_rest)**:

**Manufacturer Model**

-   This model represents automobile manufacturers.
-   It includes a field for the manufacturer's name.

**VehicleModel Model**

-   This model represents specific vehicle models produced by manufacturers.
-   It includes fields for the model name and a URL for a picture.
-   The manufacturer field establishes a foreign key relationship with the Manufacturer model, linking each model to a specific manufacturer.
-   The get_api_url method generates the URL for accessing vehicle model details via the API.

**Automobile Model**

-   This model represents automobiles within the inventory microservice.
-   It includes fields for the color, manufacturing year, VIN (Vehicle Identification Number), and a field to track whether the automobile is sold.
-   The model field establishes a foreign key relationship with the VehicleModel model, linking each automobile to a specific model.

**Integration**

-   The Automobile model in the inventory microservice references the VehicleModel model using a foreign key relationship, allowing each automobile to be associated with a specific vehicle model.

---

## Sales microservice

**SalesPerson Model**

-   This model has three total fields. First Name, Last Name and Employee Id. We used these fields in multiple parts of the project such as listing sales people, sales people history, and in recording a new sale. The employee id had to be unique so that we couldn't create a duplicate employee. Or at least they couldn't have the same id.

**Customer**

-   The customer model was used to represent customers coming in and out of our business.
-   This model had four fields first name, last name, address and phone number. With these fields phone number was also unique so that a phone numbers could only be used once throughout all our customers.
-   We used this model to list all of our customers and record a sale in the front end of our application.

**Sale**

-   This model was really the tying of all of sales other models together. It had four total fields. Price, automobile, salesperson and customer. Automobile, salesperson and customer were all foreign keys that the sales model used, because we needed an existing vin from an automobile, an existing customer and a salesperson to actually make a sale or list sales. Price was the only field that was native to the Sale model.

-   The Sale model was used on recording a sale, sales person history and to list all sales in our applications front end.

**AutomobileVO**

-   This model was a value object model that had two fields vin and sold.
-   To get data for this model we used a poller to grab data from the inventory-api for existing automobiles.
-   We then used this data when making a sale, we used the vin as a unique identifier of any automobile and we only listed said automobile if its sold field was set to false.

**Integration**

-   The Sales model having three foreign keys allowed for the relation of salesperson to a sale in our sales microservice. It also allowed for the recording of a new sale to have all the correct categories it needed to be stored which was having an already existing customer, an already existing salesperson and an automobile with a unique vin, that also has the sold field set to false, meaning that it hasn't been sold yet.

---

## How to run this project

#### 1. Fork the repo

#### 2. Clone the project

```
cd <<into your project directory>>
```

#### 3. Run the following commands in the terminal

```
docker volume create beta-data
docker compose build
docker compose up
```

##### Notes:

When you run `docker-compose up` and if you're on macOS, you will see a warning about an environment variable named OS being missing. You can safely ignore this.

## Endpoints

#### Front End

| HTTP Method | URL                                                               | Description            |
| ----------- | ----------------------------------------------------------------- | ---------------------- |
| GET         | [localhost:3000](http://localhost:3000/)                          | Home page              |
| GET         | [/automobiles](http://localhost:3000/automobiles)                 | List of Automobiles    |
| POST        | [/automobile/create](http://localhost:3000/automobile/create)     | Create an Automobile   |
| GET         | [/manufacturers](http://localhost:3000/manufacturers)             | List all Manufacturers |
| POST        | [/manufacturer/create](http://localhost:3000/manufacturer/create) | Create a Manufacturer  |
| GET         | [/models](http://localhost:3000/models)                           | List all Models        |
| POST        | [/model/create](http://localhost:3000/model/create)               | Create a Model         |
| GET         | [/technicians](http://localhost:3000/technicians)                 | List all Technicians   |
| POST        | [/technician/create](http://localhost:3000/technician/create)     | Create a Technician    |
| GET         | [/appointments](http://localhost:3000/appointments)               | List all Appointments  |
| POST        | [/appointment/create](http://localhost:3000/appointment/create)   | Create an Appointment  |
| GET         | [/service-history](http://localhost:3000/service-history)         | List all Service       |
| GET         | [/customers](http://localhost:3000/customers)                     | List all Customers     |
| POST        | [/customer/create](http://localhost:3000/customer/create)         | Create a Customer      |
| GET         | [/salespeople](http://localhost:3000/salespeople)                 | List all Salespeople   |
| POST        | [/salesperson/create](http://localhost:3000/salesperson/create)   | Create a Salesperson   |
| GET         | [/sales](http://localhost:3000/sales)                             | List all Sales         |
| POST        | [/sale/create](http://localhost:3000/sale/create)                 | Create a Sale          |
| GET         | [/salesperson-history](http://localhost:3000/salesperson-history) | History of all Sales   |

#### Service API

| HTTP Method | URL                                                                                   | Description             |
| ----------- | ------------------------------------------------------------------------------------- | ----------------------- |
| GET         | [Base URL](http://localhost:8080/)                                                    | Base page               |
| GET         | [/api/appointments](http://localhost:8080/api/appointments/)                          | List of Appointments    |
| POST        | [/api/appointments](http://localhost:8080/api/appointments/)                          | Create an Appointment   |
| DELETE      | [/api/appointments/:id](http://localhost:8080/api/appointments/:id)                   | Delete an Appointment   |
| PUT         | [/api/appointments/:id/complete](http://localhost:8080/api/appointments/:id/complete) | Complete an Appointment |
| PUT         | [/api/appointments/:id/cancel](http://localhost:8080/api/appointments/:id/cancel)     | Cancel an Appointment   |
| GET         | [/api/technicians](http://localhost:8080/api/technicians/)                            | List of Technicians     |
| POST        | [/api/technicians](http://localhost:8080/api/technicians/)                            | Create a Technician     |
| DELETE      | [/api/technicians/:id](http://localhost:8080/api/technicians/:id)                     | Delete a Technician     |

### Sales API

| HTTP Method | URL                                                               | Description          |
| ----------- | ----------------------------------------------------------------- | -------------------- |
| GET         | [Base URL](http://localhost:8090/)                                | Base page            |
| GET         | [/api/customers](http://localhost:8090/api/customers/)            | List of Customers    |
| POST        | [/api/customers](http://localhost:8090/api/customers/)            | Create a Customer    |
| DELETE      | [/api/customers/:id](http://localhost:8090/api/customers/:id)     | Delete a Customer    |
| GET         | [/api/salespeople](http://localhost:8090/api/salespeople/)        | List of Salespeople  |
| POST        | [/api/salespeople](http://localhost:8090/api/salespeople/)        | Create a Salesperson |
| DELETE      | [/api/salespeople/:id](http://localhost:8090/api/salespeople/:id) | Delete a Salesperson |
| GET         | [/api/sales](http://localhost:8090/api/sales/)                    | List of Sales        |
| POST        | [/api/sales](http://localhost:8090/api/sales/)                    | Create a Sale        |
| DELETE      | [/api/sales/:id](http://localhost:8090/api/sales/:id)             | Delete a Sale        |

#### Inventory API - Automobiles

| HTTP Method | URL                                                                   | Description              |
| ----------- | --------------------------------------------------------------------- | ------------------------ |
| GET         | [Base URL](http://localhost:8100/)                                    | Base page                |
| GET         | [/api/automobiles](http://localhost:8100/api/automobiles/)            | List of Automobiles      |
| GET         | [/api/automobiles/:vin/](http://localhost:8100/api/automobiles/:vin/) | Get an Automobile by VIN |
| POST        | [/api/automobiles](http://localhost:8100/api/automobiles/)            | Create an Automobile     |
| DELETE      | [/api/automobiles/:id](http://localhost:8100/api/automobiles/:id)     | Delete an Automobile     |
| PUT         | [/api/automobiles/:vin/](http://localhost:8100/api/automobiles/:vin/) | Update an Automobile     |
| DELETE      | [/api/automobiles/:vin/](http://localhost:8100/api/automobiles/:vin/) | Delete an Automobile     |

### Inventory API - Manufacturers

| HTTP Method | URL                                                                     | Description              |
| ----------- | ----------------------------------------------------------------------- | ------------------------ |
| GET         | [Base URL](http://localhost:8100/)                                      | Base page                |
| GET         | [/api/manufacturers](http://localhost:8100/api/manufacturers/)          | List of Manufacturers    |
| GET         | [/api/manufacturers/:id/](http://localhost:8100/api/manufacturers/:id/) | Get a Manufacturer by ID |
| POST        | [/api/manufacturers](http://localhost:8100/api/manufacturers/)          | Create a Manufacturer    |
| PUT         | [/api/manufacturers/:id/](http://localhost:8100/api/manufacturers/:id/) | Update a Manufacturer    |
| DELETE      | [/api/manufacturers/:id/](http://localhost:8100/api/manufacturers/:id/) | Delete a Manufacturer    |

### Inventory API - VehicleModels

| HTTP Method | URL                                                       | Description              |
| ----------- | --------------------------------------------------------- | ------------------------ |
| GET         | [Base URL](http://localhost:8100/)                        | Base page                |
| GET         | [/api/models](http://localhost:8100/api/models/)          | List of VehicleModels    |
| GET         | [/api/models/:id/](http://localhost:8100/api/models/:id/) | Get a VehicleModel by ID |
| POST        | [/api/models](http://localhost:8100/api/models/)          | Create a VehicleModel    |
| PUT         | [/api/models/:id/](http://localhost:8100/api/models/:id/) | Update a VehicleModel    |
| DELETE      | [/api/models/:id/](http://localhost:8100/api/models/:id/) | Delete a VehicleModel    |
