from .models import AutomobileVO, SalesPerson, Customer, Sale
from common.json import ModelEncoder

class AutomobileVOEncoder(ModelEncoder):
    model = AutomobileVO
    properties = ["vin", "sold"]

class SalesPersonEncoder(ModelEncoder):
    model = SalesPerson
    properties = ["first_name", "last_name", "employee_id", "id"]

class CustomerEncoder(ModelEncoder):
    model = Customer
    properties = ["first_name", "last_name", "address", "phone_number", "id"]

class SaleEncoder(ModelEncoder):
    model = Sale
    properties = [
        "price", 
        "automobile", 
        "salesperson", 
        "customer", 
        "id",
        ]
    encoders = {
        "automobile": AutomobileVOEncoder(),
        "salesperson": SalesPersonEncoder(),
        "customer": CustomerEncoder(),
    }